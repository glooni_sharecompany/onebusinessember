import Application from '@ember/application';
import Resolver from './resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';

const App = Application.extend({
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver
});

loadInitializers(App, config.modulePrefix);

export default App;

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomFloat(min, max, length) {
	return (Math.random() * (max - min) + min).toFixed(length);
}

//define(["LightstreamerClient", "StatusWidget"], function(LightstreamerClient, StatusWidget) {
//	var protocolToUse = document.location.protocol != "file:" ? document.location.protocol : "http:";
//	var portToUse = document.location.protocol == "https:" ? "443" : "8080";
	
//	var lsClient = new LightstreamerClient("http://lsp1.quintrics.nl", "QUINTRICS");
	
//	lsClient.connectionSharing.enableSharing("DemoCommonConnection", "ATTACH", "CREATE");
//	lsClient.addListener(new StatusWidget("left", "0px", true));
//	lsClient.connect();

//	var lsuser = $.pageParams.getCookie("LsUser"); //74427298%7c12345%7c20180116133006%7c188.95.97.118%7c182835
//	var lspw = $.pageParams.getCookie("LsPassword");//MCwCFB3KfvggIC4YYZMc7JHe8eGzMJ1eAhQgRp81lQKXC0hOQh3iJu7GvmzH0Q%3d%3d

//	client.connectionDetails.setUser(lsuser);
//	client.connectionDetails.setPassword(lspw);

//	var stocksGrid = new StaticGrid("stocksGrid", true);
//	var subscription = new Subscription("MERGE", ["MR30283", "MR266501", "MR121144", "MR238819", "MR662150", "MR1262998", "MR662760"], ["trade", "change"]);
//	subscription.setDataAdapter("MARKETDATA");
//	subscription.addListener(stocksGrid);

//	subscription.addListener({
//		onItemUpdate: function (updateInfo) {
//			var block = $("[data-item=" + updateInfo.By + "]").first().parent().parent();
//			var trade = parseFloat(updateInfo.getValue("trade"));
//			var change = parseFloat(updateInfo.getValue("change"));
//			var percent = (change / trade) * 100;
//			if (change === 0) {
//				block.removeClass("green-block red-block").addClass("grey-block");
//			} else if (change > 0 ) {
//				block.removeClass("grey-block red-block").addClass("green-block");
//			} else {
//				block.removeClass("grey-block green-block").addClass("red-block");
//			}
					
//			block.find('.indice-percchange').text(percent.toFixed(2)+'%');
//		}
//	});

//	client.subscribe(subscription);

//	client.connect();

    
//	return lsClient;
//});
