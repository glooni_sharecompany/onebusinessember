import Route from '@ember/routing/route';
let data = [{
	Name:"BBVA",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"CISCO",
	Koers: 40.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"ENI",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"IBM",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"Intel",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"PepsiCo",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"Index",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"Old Index",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"AfterIndex",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"Tiscani",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00",
}
];

setInterval(function() {
	var i = getRandomInt(0,10);
	let koers = Ember.get(data[i],'Koers');
	var valueChanged = getRandomFloat(-1,2,2);
	valueChanged = parseFloat(valueChanged);
	koers = parseFloat(koers + valueChanged).toFixed(2);
	Ember.set(data[i],'Koers', koers);
	Ember.set(data[i],'ValueChanged', valueChanged);
	Ember.get(data[i],'Koers');
	Ember.set(data[i],'Perc', getRandomFloat(0,1,2));
	Ember.set(data[i],'Hoog', getRandomFloat(400,600,2));
	Ember.set(data[i],'Laag', getRandomFloat(350,550,2));
	Ember.set(data[i],'Tijd', new Date().toTimeString().split(' ')[0]);

	if(valueChanged > 0){
		Ember.set(data[i],'Positive','tick-up');
		Ember.set(data[i],'Sign','up');
	}
	else if(valueChanged < 0){
		Ember.set(data[i],'Positive','tick-down');
		Ember.set(data[i],'Sign','down');
	}
	else{
		Ember.set(data[i],'Sign','equals');
		Ember.set(data[i],'Positive','');
	}
	setTimeout(function() {
		Ember.set(data[i],'Positive','');
	},1999);
}, 2000);

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomFloat(min, max, length) {
	return (Math.random() * (max - min) + min).toFixed(length);
}

export default Route.extend({
	model(){ return data;}
});
