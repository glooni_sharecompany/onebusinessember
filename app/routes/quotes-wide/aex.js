﻿import Route from '@ember/routing/route';
let data = [{
	Name:"AEX",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"Aalberts",
	Koers: 40.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"ABN AMRO",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"Aegon",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"ASML",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"DSM",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"KPM",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"RELX",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"AEX-Index",
	Koers: 540.360,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00"
},{
	Name:"Vopak",
	Koers: 520.311,
	ValueChanged:0.460,
	Perc:0.09,
	Hoog:542.740,
	Laag:540.320,
	Omzet:0,
	Tijd:"16:57:00",
}
];

setInterval(function() {
	var i = getRandomInt(0,10);
	let koers = Ember.get(data[i],'Koers');
	var valueChanged = getRandomFloat(-1,2,2);
	valueChanged = parseFloat(valueChanged);
	koers = parseFloat(koers + valueChanged).toFixed(2);
	Ember.set(data[i],'Koers', koers);
	Ember.set(data[i],'ValueChanged', valueChanged);
	Ember.set(data[i],'Perc', getRandomFloat(0,1,2));
	Ember.set(data[i],'Hoog', getRandomFloat(400,600,2));
	Ember.set(data[i],'Laag', getRandomFloat(350,550,2));
	Ember.set(data[i],'Tijd', new Date().toTimeString().split(' ')[0]);

	if(valueChanged > 0){
		Ember.set(data[i],'Positive','tick-up');
		Ember.set(data[i],'Sign','up');
	}
	else if(valueChanged < 0){
		Ember.set(data[i],'Positive','tick-down');
		Ember.set(data[i],'Sign','down');
	}
	else{
		Ember.set(data[i],'Sign','equals');
		Ember.set(data[i],'Positive','');
	}
	setTimeout(function() {
		Ember.set(data[i],'Positive','');
	},1999);
}, 2000);
function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomFloat(min, max, length) {
	return (Math.random() * (max - min) + min).toFixed(length);
}

export default Route.extend({
	model(){ return data;}
});
