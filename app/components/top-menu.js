import Component from '@ember/component';

export default Component.extend({
	aexSelected:true,
	amxSelected:false,
	ascxSelected:false,
	overigAmsterdamSelected:false,
	actions : {
		SelectMenuItem(name){
			if(name == 'amxSelected'){
				this.set('amxSelected',true);
				this.set('aexSelected', false);
				this.set('ascxSelected', false);
				this.set('overigAmsterdamSelected', false);
			}else
				if( name == 'ascxSelected'){
					this.set('ascxSelected', true);
					this.set('amxSelected',false);
					this.set('aexSelected', false);
					this.set('overigAmsterdamSelected', false);
				}else
					if( name == 'overigAmsterdamSelected'){
						this.set('overigAmsterdamSelected', true);
						this.set('amxSelected',false);
						this.set('aexSelected', false);
						this.set('ascxSelected', false);
					}else{
						this.set('aexSelected', true);
						this.set('overigAmsterdamSelected', false);
						this.set('amxSelected',false);
						this.set('ascxSelected', false);
					}
		}
	}
});
