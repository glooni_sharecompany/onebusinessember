import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('iframelist');
  this.route('newshome');
  this.route('newspage');
  this.route('profile');
  this.route('quotes-wide', function() {
    this.route('aex');
    this.route('amx');
    this.route('ascx');
    this.route('overigamsterdam');
  });
});

export default Router;
